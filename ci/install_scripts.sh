#!/bin/bash

function install_silx(){

    if [ "$2" = 'latest' ]; then
        python -m install silx
    else
        python -m pip install git+https://github.com/silx-kit/silx.git
    fi
}


function silx_version(){
    python -c 'import silx; print(silx.version)'
}
