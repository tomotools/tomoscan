"""General utils"""

from .decorator import docstring  # noqa F401
from .geometry import BoundingBox1D, BoundingBox3D, get_subvolume_shape  # noqa F401
