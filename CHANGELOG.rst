Change Log
==========

2.1.1: 2024/11/28
-----------------

* volume:
    * VolumeSingleFrameBase: add cleaning of possible remaining files from previous save (PR 243)

2.1.0: 2024/11/22
-----------------

* scan
    * remove 'HDF5XRD3DScan' and 'XRD3DFrame' classes
* series
    * deprecate tomoscan.serie.serie (PR 236)

2.0.0: 2024/02/07
-----------------

* scan:
    * NXtomo: replace `x_flipped` and `y_flipped` by NXtransformations (PR 145)
    * deprecate `tomoscan.esrf.scan.hdf5scan.HDF5TomoScan` in favor of `tomoscan.esrf.scan.nxtomo.NXtomoScan`
    * deprecate `tomoscan.esrf.scan.mock.MockHDF5` in favor of `tomoscan.esrf.scan.mock.MockNXtomo`
    * deprecate `tomoscan.test.utils.HDF5MockContext` in favor of `tomoscan.test.utils.NXtomoMockContext`
    * add 'build_drac_metadata' function
    * NXtomoScan.flat_n: change behavior: return the number of flat per series (like for EDFTomoScan) instead of the total number of (raw) flats
* add dependancy to `nxtomo` and move all nexus path to it.


1.3.2: 2023/08/23
-----------------

* volume
    * jp2k: rescale data if cast it to uint16 by default


1.3.0: 2023/08/01
-----------------

* identifier
    * replace `data_path` request by `path` to be closer to silx API (PR 144)
* volume
    * replace `pixel_size`-scalar by `voxel_size`-tuple. (PR 139)
    * `get_bounding_box`: remove API allowing to provide axis as 'x', 'y', 'z' and only tolerate integers (PR 138)
    * `guess_volumes`: add filtering of nabu histograms (8444be27)
* scan:
    * HDF5TomoScan:
        * modify NXtomo behavior when no image_key_control: previously it was trying to get some 'return' projections from rotation angle. This was a non sense. It has been removed.
        * check detector dataset - log warning if virtual dataset is broken or appear to contain more external file than the OS can handle (PR 147)
* misc
    * increase usage of f-strings when possible (PR 71)


1.2.0: 2023/02/23
-----------------

* volume:
    * single frame per file volume (edf, jp2k) default digit number moved from 4 to 6 (reading still compatible - PR 336)
* frame reducer
    * overwrite existing dataset when possible (PR 128)
* scan
    * rework get_short_description: compress file name if too long (PR 129)
* misc
    * remove distutils for packaging (PR 126)

1.1.2: 2023/01/10
-----------------

* volume:
    * add RawVolume (legacy for some processing pipeline - PR 123)

1.1.0: 2022/12/15
-----------------

* scan: add metadata for stitching (!118)
* misc: add pylint

1.0.0: 2022/08/30
-----------------

* handle source probe (PR 80)
* volume:
    * add `browse_data_urls` function (PR 83)
    * add `browse_data_files` and `browse_metadata_files` functions (PR 82)
    * create output directories for single files if doesn't exists (PR 81)
    * add utils function (`guess_volumes`, `guess_hdf5_volume_data_paths`...) (PR 85, PR 89)
    * HDF5 Volume: add `browse_slices` and `load_chink` functions (PR 96)
* reduced frames
    * add metadata to reduced frames (PR 92)
    * add `copy_darks_to` and `copy_flats_to` functions (PR 90)
    * add utils to handle relative reduced frame indices (PR 99)
* validator
    * use `tomo_n` when possible on NXtomo validity check (PR 97)
* misc
    * rename `exposure_time` to `count_time` (PR 98)

0.9.0: 2022/06/19
-----------------

* EDFScan: add `dataset_basename` and `scan_info` to the constructor (PR 77)
    * `dataset_basename` is used to determine the projection files. If not provided fall back on folder name (as it was the case previously. Insure compatibility if not provided)
    * `scan_info`: users can provide .info file to be used to retrieve metadata. If not provided uses `datasaet_basename`.info. If not provided insure to have the same behavior as previously.

* add machine current (PR 78)
    * goes to `nexus_path.ELECTRIC_CURRENT_PATH` for HDF5 / nexus
    * goes to `nexus_path.ELECTRIC_CURRENT_PATH` for HDF5 / nexus

0.8.0: 2022/02/05
-----------------

* add nexus module: module dedicated to defines nexus path (PR 73)
* unitsystem
    * add timesystem. Module dedicated to time system (PR 72)
* improve management of a series (PR 67)
* add the volume of concept inside tomoscan (PR 70)

0.7.0: 2022/01/04
-----------------

* esrf
    * add default methods to load and save darks / flats according to the file format (hdf5, edf): PR 65
    * improve mock class: PR 57
    * hdf5scan
        * add possibility to provide the nexus version on the constructor to insure backward compatibility: PR 58
    * add 'get_relative_file' helper.
* Miscellaneous
    * move documentation to autosummary: PR 56
    * replace some 'ref' by 'flat': PR 60

0.6.0: 2021/08/31
-----------------

* add validator: helper function to parse input file and look for some missing dataset, invalid values or broken link
* esrf
    * TomoScanBase
        * add a first version of the intensity normalization
        * add property `intensity_monitor` to read diode for example
        * add property `exposure_time` to get frame exposure time
    * add `Source` class
    * move _FOV to public
    * HDF5TomoScan
        * adapt to read from various version of nexus version
            * add `_NEXUS_PATHS` class to keep trace of the path through history and simplify reading from a specific version
        * `tomo_n` now return the value from `instrument/detector/tomo_n` instead of len(projections)
    * utils
        * get_compacted_dataslices: preserve defined scheme 
    * add `HDF5XRD3DScan` class to handle XRD-3D data

* Miscellaneous
    * move to setup.cfg

0.5.0: 2021/04/20
-----------------

* esrf
    * TomoScanBase
        * add `sequence_name`, `sample_name` and `group_size` properties
        * add `start_time` and `end_time` properties
        * add `x_translation`, `y_translation` and `z_translation` properties (Not handled by EDFTomoScan for now)
        * HDF5TomoScan
            * speed up sinogram generation
        EDFTomoscan
            * improve lazy loading
    * utils: add the concept of groups. A group for example is a set of sequences defining a zseries

0.4.0: 2020/11/09
-----------------

* esrf
    * HDF5TomoScan & EDFTomoScan add ignore_projections in the constructor
    * add properties to scanBase.TomoScanBase class:
        * alignment_projections
        * estimated_cor_frm_motor

    * add flat_field_correction function
    * move h5py dependency to 3.x

* Miscellaneous: add 'black' format

0.3.2: 2020/08/26
-----------------

* HDF5TomoScan
    * manage hdf5 files using the HDFFile Class in order to avoid any file corruption.

0.3.1: 2020/08/19
-----------------

* TomoScanBase
    * add field_of_view

* notebooks: rework notebooks

0.3.0: 2020/06/26
-----------------

* move default distance units to meter

* open with swmr mode by default

* utils:
    * add get_compacted_dataslices
    * rework HDF5 mock class


0.2.0: 2020/03/09
-----------------

* add some cache

* TomoScanBase
    * add distance
    * add energy

* EDFTomoScan
    * rework guess_index_frm_file_name

* HDF5TomoScan
    * add entry (one master file can have several sequence so several HDF5TomoScan)
    * add image_key (projection, dark...)
    * adapt to latest bliss file (bamboo)
    * add return_projs
    * add rotation_angle
    * add magnified pixel size
    * add frames

* rework UnitSystem


0.1.0: 2020/02/11
-----------------

* add tomoscan.esrf.edfscan

* add tomoscan.esrf.hdf5scan
