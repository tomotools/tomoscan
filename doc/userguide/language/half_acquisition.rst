Standard acquisition vs half acquisition
----------------------------------------

By default a 'standard' acquisition looks like:

    .. image:: img/standard_acquisition.png
        :align: center


then we can acquire on a range of 180 or 360 degree in order to run a reconstruction.

But sometime (large sample) an acquisition can be made with only half of the sample in the field of view like:

    .. image:: img/half_acquisition.png
        :align: center


In this case we need to have a 360 degree acquisition and rework (stick) sinograms in order to be able to run the reconstruction.


.. warning:: We expect to get this information for bliss files. But for EDF it does not exists. So to try to guess it if this is 360 acquisition then we expect it to be a half acquisition.
             And if it has a scan_range of 180 we expect it to be a standard acquisition. But in many case this 'guess' can be wrong. So for EDF the safer is to provide this information to nabu / tomwer.
