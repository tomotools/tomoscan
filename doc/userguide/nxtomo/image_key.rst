'image_key' and 'image_key_control'
"""""""""""""""""""""""""""""""""""

`NXtomo application <https://manual.nexusformat.org/classes/applications/NXtomo.html#nxtomo>`_ is defining the format to be used to handle NXtomo.

In order to know the frame type (dark field, flat field, projection...) we use the `image_key` dataset.
It gives an int value for each frame:

* projection = 0
* flat field = 1
* dark field = 2
* invalid = 3

But on our case we want to distinguish some specific projection which are the alignment or return projections. So projections done again at a specific angle for alignment, check if the sample has moved...
As this was not part of the initial application we created the 'image_key_control' dataset which can contain a -1 value for those projections.

Now the format allows to provide negative values to `image_key`. But until now we have keep the two datasets.

.. note:: to interprete an NXtomo we first try to access `image_key_control` else we fall back on `image_key`. This process ensure compatibility with any NXtomo

.. hint:: to edit values of `image_key` there is several options. The first one is that you create your own python script and edit the file. The second one is to use the `nxtomo library <https://gitlab.esrf.fr/tomotools/nxtomo>`_. `Here <https://tomotools.gitlab-pages.esrf.fr/nxtomo/tutorials/edit.html>`_ is a tutorial presenting the edition of an NXtomo with this library.
    There is also a tomwer widget to achieve it. `See the video <https://www.youtube.com/watch?v=4yrEBrAmwtY&list=PLddRXwP6Z6F8dL0fcRM7g_TOOezuh8Cvy>`_
