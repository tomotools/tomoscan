.. _identifiers:


identifiers (url)
-----------------

identifiers are a convenient way to uniquely identify an object along the tomotools.
It is not used everywhere but we intend to use it more and more.

For example to define a scan / volume from a Command Line Interface CLI, a python script or to share an object between processes.

For now there is two families of objects in tomoscan (aka `tomo_type`):

* scan(s)
* volume(s)

Scans and volumes can use different scheme to be saved (hdf5, edf...). Finally they are identified by a location like a file and a data path for HDF5 or a folder for EDF.
The general definition is based on the `url definition <https://en.wikipedia.org/wiki/URL>`_ and can be interprated for tomography as:

.. code-block:: bash

    scheme:tomo_type:path[?queries]

.. _scan identifiers:

scan identifiers
````````````````

HDF5Scan (NXtomo) identifier
''''''''''''''''''''''''''''

url is defined as:

.. code-block:: bash

    hdf5:scan:{path_to_file}?data_path={hdf5_group_path}

and here is a 'real life' example:

.. code-block:: bash

    hdf5:scan:/tmp_14_days/payno/bambou_hercules_0001/bambou_hercules_0001.nx?data_path=entry0000

or from a Python / NXtomoScan point of view we could have:

.. code-block:: python

    from from tomoscan.esrf.scan.nxtomoscan import NXtomoScan
    scan = NXtomoScan("/tmp_14_days/payno/bambou_hercules_0001/bambou_hercules_0001.nx", "entry0000")
    scan.get_identifier().to_str()
    > 'hdf5:scan:/mnt/multipath-shares/tmp_14_days/payno/bambou_hercules_0001/bambou_hercules_0001.nx?data_path=entry0000'

EDFScan (spec) identifier
'''''''''''''''''''''''''

url is defined as:

.. code-block:: bash

    edf:scan:{path_to_folder}?file_prefix={file_prefix}

and here is a 'real life' example:

.. code-block:: bash

    edf:scan:/media/payno/830add5c-f5d7-4db2-875e-03be4f0a67f0/dataset/tomography/tomwerExtraDatasets/001_0.28_19keV_Al63

.. note:: the file_prefix is optional, by default he will take the name of the folder as the file prefix. This is the same for the `info_file`. 

or from a Python / EDFTomoScan point of view we could have:

.. code-block:: python

    from tomoscan.esrf.scan.edfscan import EDFTomoScan
    scan = EDFTomoScan("/media/payno/830add5c-f5d7-4db2-875e-03be4f0a67f0/dataset/tomography/tomwerExtraDatasets/001_0.28_19keV_Al63")
    scan.get_identifier().to_str()
    > 'edf:scan:/media/payno/830add5c-f5d7-4db2-875e-03be4f0a67f0/dataset/tomography/tomwerExtraDatasets/001_0.28_19keV_Al63?file_prefix=001_0.28_19keV_Al63'

.. _volume identifiers:

volume identifiers
``````````````````

first you should be aware that the 'volume.ipynb' notebook is explaining how to save different types of volumes and how the identifiers are working. This can be a more advanced source of information for volumes identifiers.

Multi frame per file volume
'''''''''''''''''''''''''''

This category groups all volumes which store all the frames into a single file. For now this include :class:`HDF5Volume` and :class:`MultiTIFFVolume`.

HDF5Volume identifier
^^^^^^^^^^^^^^^^^^^^^

url is defined as:

.. code-block:: bash

    hdf5:volume:{path_to_file}?data_path={hdf5_group_path}

here is an example:

.. code-block:: bash

    hdf5:volume:/path/to/file.h5?data_path=/entry0000/reconstruction

MultiTIFFVolume identifier
^^^^^^^^^^^^^^^^^^^^^^^^^^

url is defined as

.. code-block:: bash

    tiff3d:volume:{path_to_folder}

real life example

.. code-block:: bash

    tiff3d:volume:/path/to/folder/

Single frame per file volume
''''''''''''''''''''''''''''

This category groups all volumes which store one frame per file as :class:`EDFVolume`, :class:`TIFFVolume` and :class:`JP2KVolume`

single frame identifier are following this 'architecture':

.. code-block:: bash

    scheme:volume:{path_to_folder}?file_prefix={file_prefix}

.. note:: file prefix is optional. If not defined then he will take the name of the output folder

EDFVolume identifier
^^^^^^^^^^^^^^^^^^^^

url is defined as:

.. code-block:: bash
    
    hdf5:volume:{path_to_folder}?file_prefix={file+prefix}

real life example:

.. code-block:: bash
    
    edf:volume:/media/payno/830add5c-f5d7-4db2-875e-03be4f0a67f0/dev/tomography/tomoscan/doc/tutorials/edf_volume

TIFFVolume identifier
^^^^^^^^^^^^^^^^^^^^^

url is defined as: 

.. code-block:: bash

    tiff:volume:{path_to_folder}?file_prefix={file_prefix}

real life example:

.. code-block:: bash

    tiff:volume:/tmp/test_volume_with_prefix_TIFFVo0/acquisition/volume?file_prefix=test

JP2KVolume identifier
^^^^^^^^^^^^^^^^^^^^^

url is defined as:

.. code-block:: bash

    jp2k:volume:{path_to_folder}?file_prefix={file_prefix}
