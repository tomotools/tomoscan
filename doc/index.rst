tomoscan
========

tomoscan aims to provide an unified interface to read tomography data from several file format.
For now it can handle:

* acquisitions (scan):
   * nexus format (using `NXtomo <https://manual.nexusformat.org/classes/applications/NXtomo.html#nxtomo>`_ - `HDF5 <https://www.hdfgroup.org/solutions/hdf5/>`_)
   * spec - format (using `edf <https://fr.wikipedia.org/wiki/European_data_format>`_)
* volumes:
   * single frame file:
      * EDF
      * JP2K
      * tiff
   * multi frame:
      * HDF5
      * multitiff

.. note:: You can also convert EDF dataset to obtain an HDF5 - NXTomo compliant file by using `nxtomomill <https://gitlab.esrf.fr/tomotools/nxtomomill>`_.
          tomoscan will not produce any other file.

tomoscan is one of the `tomotools <https://gitlab.esrf.fr/tomotools>`_ developed at the `european synchrotron radiation facility <http://www.esrf.fr>`_

.. toctree::
   :hidden:

   tutorials/index.rst
   api.rst
   userguide/index.rst
   development/index.rst
