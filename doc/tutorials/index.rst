Tutorials
---------

.. toctree::
   :maxdepth: 1
   :hidden:

   edf_scan
   hdf5_tomo_scan
   volume


.. grid:: 3

   .. grid-item-card::
      :img-top: img/edf.svg

      :doc:`edf_scan`

   .. grid-item-card::
      :img-top: img/nxtomo.svg

      :doc:`hdf5_tomo_scan`

   .. grid-item-card::
      :img-top: img/volumes.svg

      :doc:`volume`

   .. grid-item-card::
      :img-top: img/publish_to_drac.svg

      :doc:`publish_processed_data_to_data_portal`
